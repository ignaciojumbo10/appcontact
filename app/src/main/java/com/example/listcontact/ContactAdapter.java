package com.example.listcontact;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.listcontact.database.Contact;

import java.util.List;

public class ContactAdapter extends ArrayAdapter {

    //creacion del objeto de tipo Contact
    Contact contact;


    private Context context;
    private List<Contact> contactArrayList;
    public ContactAdapter(Context context, List<Contact> contactArrayList){
        super(context, R.layout.list_contact);
        this.context = context;
        this.contactArrayList = contactArrayList;

    }
    @Override
    public int getCount() {
        return contactArrayList.size();
    }

    @Override
    public Contact getItem(int position) {
        return contactArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    // este metodo permite obtener la vista a partir de una fila determinada(posicion)
    // la vista que representa la lista se asa por parametro al metodo(convertView)
    // se parasa
    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        View view;
        final  ViewHolder viewHolder;
        if(convertView == null || convertView.getTag() == null){
            viewHolder = new ViewHolder();
            view = LayoutInflater.from(context).inflate(R.layout.list_contact,parent, false);
            viewHolder.mContactName = view.findViewById(R.id.adapterContactName);
            viewHolder.mImageContact = view.findViewById(R.id.adapterContactImage);
            view.setTag(viewHolder);

        }else{
            viewHolder =(ViewHolder)convertView.getTag();
            view =convertView;
        }
        viewHolder.mContactName.setText(contactArrayList.get(position).getNombre());

        Glide.with(context)
                //seteo la imagen
                .load(contactArrayList.get(position).getUrl())
                .into(viewHolder.mImageContact);
    return view;
    }
    static  class ViewHolder{
        protected TextView mContactName;
        protected ImageView mImageContact;
    }

}
