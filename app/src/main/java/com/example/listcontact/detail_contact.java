package com.example.listcontact;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.example.listcontact.database.Contact;
import com.example.listcontact.database.ContactLab;

public class detail_contact extends AppCompatActivity implements View.OnClickListener {
    private TextView ubicacion, name, descripcion, telefono;
    private ImageView ima;
    private ImageButton btnEliminar;
    private ContactLab contactLab;
    Contact contact;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detail_contact);

        //obtengo los datos del main activity
        String direccion = getIntent().getStringExtra("direccion");
        String nombre = getIntent().getStringExtra("nombre");
        String des = getIntent().getStringExtra("descripcion");
        String imagen = getIntent().getStringExtra("imagen");
        String phone = getIntent().getStringExtra("telefono");

        Bundle bundle = getIntent().getExtras();
        contact = (Contact) bundle.getSerializable("contacto");


        //
        ubicacion = findViewById(R.id.detailContactCity);
        name = findViewById(R.id.detailContactName);
        descripcion = findViewById(R.id.detailContactDescription);
        ima = findViewById(R.id.detailContactImage);
        telefono = findViewById(R.id.telefono);
        btnEliminar = findViewById(R.id.btnEliminar);

        //le envio el contexto al ContactLab
        contactLab = new ContactLab(this);

        //le doy la accion el boton

        btnEliminar.setOnClickListener(this);


        //seteo los datos obtenidos
        name.setText(nombre);
        ubicacion.setText(direccion);
        descripcion.setText(des);
        telefono.setText(phone);

        //seteo la imagen a la vista
        Glide.with(this)
                //seteo la imagen
                .load(imagen)
                .into(ima);
    }

    @Override
    public void onClick(View v) {
        if (btnEliminar == btnEliminar){
            contactLab.delete(contact);
            Toast.makeText(this, "Eliminar", Toast.LENGTH_SHORT).show();
            finish();
        }

    }
}