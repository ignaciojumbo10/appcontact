package com.example.listcontact;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.listcontact.database.Contact;
import com.example.listcontact.database.ContactLab;
import com.google.firebase.analytics.FirebaseAnalytics;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private ListView list;
    private TextView textView;
    private ImageButton bnAgregar;
    private ContactLab contactLab;
    private FirebaseAnalytics mFirebaseAnalytics;
    ArrayList<Contact> arrayList = new ArrayList<>();
    ContactAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        list = findViewById(R.id.listView);
        textView = findViewById(R.id.textViewNoItems);

        contactLab = new ContactLab(this);

        llenarDatosCodigo();
        if(arrayList != null && arrayList.size() > 0){
            textView.setVisibility(View.GONE);
            list.setVisibility(View.VISIBLE);
            adapter = new ContactAdapter(this,arrayList);
            list.setAdapter(adapter);
            list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Contact selectItem =(Contact) adapter.getItem(position);
                    // Toast.makeText(getApplicationContext(), "ok ", Toast.LENGTH_LONG).show();
                    //Creo el intent para cambiar de actividad y envio los datos
                    //contactLab.delete(selectItem);
                    Intent intent = new Intent(getApplicationContext(), detail_contact.class);
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("contacto",selectItem);
                    intent.putExtras(bundle);
                    intent.putExtra("nombre", selectItem.getNombre());
                    intent.putExtra("telefono", selectItem.getTelefono());
                    intent.putExtra("direccion", selectItem.getDescripcion());
                    intent.putExtra("descripcion", selectItem.getCiudad());
                    intent.putExtra("imagen", selectItem.getUrl());
                    startActivity(intent);
                }
            });


        }else{
            Toast.makeText(this, "No hay registros", Toast.LENGTH_SHORT).show();

           // list.setVisibility(View.GONE);
          //  textView.setVisibility(View.VISIBLE);
        }


        //
        bnAgregar = findViewById(R.id.bnAgregar);
        bnAgregar.setOnClickListener(this);

    }

    private void llenarDatosCodigo(){
        arrayList.clear();
        try {
            arrayList.addAll(contactLab.getContactos());

        }catch (Exception e){

        }




    }

    // con este metodo llenos los datos desde la base de datos
    private  void llenarDatosBase(ArrayList <Contact> list){

      list.clear();
      list.addAll(contactLab.getContactos());
    }

    @Override
    public void onClick(View v) {
        if(bnAgregar == v){
            //Toast.makeText(this, "ok", Toast.LENGTH_LONG).show();
            Intent intent = new Intent(this, registrarContacto.class);
            startActivity(intent);


        }
    }
    protected void onRestart(){
        super.onRestart();
        llenarDatosCodigo();
        adapter.notifyDataSetChanged();

    }
}