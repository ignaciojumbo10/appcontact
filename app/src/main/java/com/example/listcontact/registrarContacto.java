package com.example.listcontact;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.listcontact.database.Contact;
import com.example.listcontact.database.ContactLab;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class registrarContacto extends AppCompatActivity implements View.OnClickListener {
    private Button btnGuardar;
    private Button btnCancelar;
    private EditText txtnombre, txttelefono, txtciudad, txtdescripcion, txturl;
    private Contact contacto;
    private ContactLab contactLab;

    DatabaseReference database;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrar_contacto);


        //relacion de la parte logica con la parte visual
        btnCancelar = findViewById(R.id.bnCancelar);
        btnGuardar = findViewById(R.id.bnguardarC);

        txtnombre = findViewById(R.id.txtnombre);
        txttelefono = findViewById(R.id.txtTelefono);
        txtciudad = findViewById(R.id.txtciudad);
        txtdescripcion = findViewById(R.id.txtdescripcion);
        txturl = findViewById(R.id.txtimagen);

        //le pongo un icono a lado del textView
        txtnombre.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_baseline_account_box_24,0,0,0);
        txttelefono.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_baseline_local_phone_24,0,0,0);
        txtciudad.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_baseline_location_searching_24,0,0,0);
        txtdescripcion.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_baseline_description_24,0,0,0);
        txturl.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_baseline_image_24,0,0,0);

        //le doy la accion al boton
        btnGuardar.setOnClickListener(this);
        btnCancelar.setOnClickListener(this);

        database = FirebaseDatabase.getInstance().getReference("Contact");

    }

    public void GuardarContactoFireBase(){
        String nombre = txtnombre.getText().toString();
        String telefono = txttelefono.getText().toString();
        String ciudad = txtciudad.getText().toString();
        String descripcion = txtdescripcion.getText().toString();
        String url = txturl.getText().toString();

        if(!nombre.isEmpty()){
            String id = database.push().getKey();
            contacto = new Contact(id, nombre, telefono, ciudad,descripcion,url);
            database.child("Datos").child(id).setValue(contacto);
            Toast.makeText(this, "Guardado con exito", Toast.LENGTH_SHORT).show();
        }else{
            Toast.makeText(this, "Error", Toast.LENGTH_SHORT).show();
        }



    }

    public void GuardarContactoRoom(){

        String nombre = txtnombre.getText().toString();
        String telefono = txttelefono.getText().toString();
        String ciudad = txtciudad.getText().toString();
        String descripcion = txtdescripcion.getText().toString();
        String url = txturl.getText().toString();

        Contact contact1 = new Contact();
        contact1.setNombre(nombre);
        contact1.setTelefono(telefono);
        contact1.setCiudad(ciudad);
        contact1.setDescripcion(descripcion);
        contact1.setUrl(url);

        contactLab= new ContactLab(this);
        contactLab.addContacto(contact1);
        Toast.makeText(this, "Guardado en room", Toast.LENGTH_SHORT).show();

    }


    @Override
    public void onClick(View v) {
        if (v == btnGuardar){
            GuardarContactoFireBase();
            GuardarContactoRoom();
           finish();


        }
        if(btnCancelar == v){
            finish();
        }
    }
}