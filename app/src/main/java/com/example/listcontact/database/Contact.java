package com.example.listcontact.database;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity (tableName = "contacto")

public class Contact implements Serializable {
    private String id;


    @PrimaryKey(autoGenerate = true)
    @NonNull
    private int idRoom;

    @ColumnInfo(name = "nombre")
    private String nombre;

    @ColumnInfo(name = "telefono")
    private String telefono;

    @ColumnInfo(name = "descripcion")
    private String descripcion;

    @ColumnInfo(name = "ciudad")
    private String ciudad;

    @ColumnInfo(name = "url")
    private String url;

    public Contact() {
    }

    public Contact(String id, String nombre, String telefono, String descripcion, String ciudad, String url) {
        this.id = id;
        this.nombre = nombre;
        this.telefono = telefono;
        this.descripcion = descripcion;
        this.ciudad = ciudad;
        this.url = url;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public int getIdRoom() {
        return idRoom;
    }

    public void setIdRoom(int idRoom) {
        this.idRoom = idRoom;
    }
}
