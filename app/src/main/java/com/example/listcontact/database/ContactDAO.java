package com.example.listcontact.database;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface ContactDAO {
    @Query("Select * from contacto")
    List<Contact> getContactos();

    @Query("SELECT * FROM CONTACTO WHERE ID LIKE :uuid")
    Contact getContacto(String uuid);

    @Insert
    Long addContacto(Contact c);

    @Delete
    void deleteContacto(Contact c);

    @Update
    void updateContact(Contact c);

    @Query("DELETE FROM CONTACTO")
    void deleteAllContacto();

}
